﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEditorSettings : ScriptableObject
{
#if UNITY_EDITOR
    [HideInInspector]
    public Color brushColor = Color.white;
    [HideInInspector]
    public float brushSize = 1.0f;
    [HideInInspector]
    public float maxBrushSize = 10.0f;
    [HideInInspector]
    public bool fillMode = true;
    [HideInInspector]
    public float fillModeRate = 0.25f;
    [HideInInspector]
    public string[] brushMode = new string[] { "Рисовать", "Удалять" };
    [HideInInspector]
    public LayerMask brushLayer;
    [HideInInspector]
    public bool randomRotatedChanks = false;
    [HideInInspector]
    public int[] rotateValues = new int[4] { 90, 180, 270, 360 };
    [HideInInspector]
    public Vector2 megaChankSize = new Vector2(6, 6);
    [HideInInspector]
    public bool randomRotatedSpawnObjects = false;

    [Range(0,90)]
    public float fixedAngleValue = 15.0f;

    public Color backgroundColor = new Color32(177,180,119,128);
    public Color nonSelectedObjectToSpawnColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
    public Color selectedObjectToSpawnColor = Color.green;
    public Color nonSelectedObjectToSpawnDestructionColor = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.2f);
    public Color selectedObjectToSpawnDestructionColor = Color.yellow;
    public bool showSpawnObjectsNames = false;
    public bool showChanksAndMaterialsNames = false;
    public bool attachObjectsToChank = false;

    public KeyCode keyToPlusRotate = KeyCode.E;
    public KeyCode keyToMinusRotate = KeyCode.Q;
    public KeyCode keyToRandomRotate = KeyCode.R;
    public KeyCode keyToNullRotate = KeyCode.N;
    public KeyCode keyToFillMode = KeyCode.F;

    public KeyCode keyToUndo = KeyCode.Z;
    public KeyCode keyToRedo = KeyCode.Y;

    //Locations prefabs and status
    [HideInInspector]
    public List<GameObject> prefabsToPaintAllLocations = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusAllLocations = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation1 = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation1 = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation2 = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation2 = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation3 = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation3 = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation4 = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation4 = new List<bool>();

    /// <summary>
    /// Destruction
    /// </summary>
    [HideInInspector]
    public List<GameObject> prefabsToPaintAllLocationsDestruction = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusAllLocationsDestruction = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation1Destruction = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation1Destruction = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation2Destruction = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation2Destruction = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation3Destruction = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation3Destruction = new List<bool>();
    [HideInInspector]
    public List<GameObject> prefabsToPaintLocation4Destruction = new List<GameObject>();
    [HideInInspector]
    public List<bool> prefabsToPaintStatusLocation4Destruction = new List<bool>();

    [HideInInspector]
    public List<GameObject> chanks = new List<GameObject>();
    [HideInInspector]
    public List<bool> chanksStatus = new List<bool>();
    [HideInInspector]
    public List<Material> chanksMaterial = new List<Material>();
    [HideInInspector]
    public List<bool> chanksMaterialStatus = new List<bool>();

    [Range(0,1)]
    public float previewObjectsImageZoom = 1;
    [Range(5,20)]
    public int limitHistoryActions = 10;

    [HideInInspector]
    public int modeIndex = 0;

    //[HideInInspector]
    public List<MapEditor.SpawnHistory> historySpawn = new List<MapEditor.SpawnHistory>();
    //[HideInInspector]
    public List<MapEditor.DeleteHistory> historyDelete = new List<MapEditor.DeleteHistory>();

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}*/

#endif
}
