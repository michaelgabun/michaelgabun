﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;

[ExecuteInEditMode]
public class MapEditor : EditorWindow
{

    Vector2 scrollPos;

    /// <summary>
    /// Foldout variables
    /// </summary>
    AnimBool showPaint;
    string statusPaint = "Show";

    AnimBool showObjectsToSpawn;
    string statusObjectsToSpawn = "Show";

    AnimBool showChanksSettings;
    string statusChanksSettings = "Show";


    /// <summary>
    /// Paint
    /// </summary>
    bool enablePaint = false;
    string paintMode = "Start Paint";
    public Color brushColor = Color.white;
    public float size = 5.0f;
    public float sizeMax = 10.0f;
    public float rate = 0.25f;
    public float timer = 0.0f;
    public bool fillMode = true;
    public Vector3 posBrush;
    public Vector3 dirBrush;
    public Vector3 surfaceNormBrush;

    public string[] brushMode = new string[] {"Рисовать","Удалять" };
    public int modeIndex = 0;
    public LayerMask brushLayer;

    [SerializeField]
    int _editorHash;

    public bool randomRotatedChanks = false;
    public int[] rotateValues = new int[4] { 90,180,270,360};
    public Vector2 megaChankSize = new Vector2(6,6);
    public string megaChankName = "MegaChank Template";

    public bool randomRotatedSpawnObjects = false;

	/// <summary>
	/// Static prefabs
	/// </summary>

    public List<GameObject> prefabsToPaintAllLocations = new List<GameObject>();
    public List<bool> prefabsToPaintStatusAllLocations = new List<bool>();

    public List<GameObject> prefabsToPaintLocation1 = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation1 = new List<bool>();

    public List<GameObject> prefabsToPaintLocation2 = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation2 = new List<bool>();

    public List<GameObject> prefabsToPaintLocation3 = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation3 = new List<bool>();

    public List<GameObject> prefabsToPaintLocation4 = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation4 = new List<bool>();

    /// <summary>
    /// Destruction prefabs
    /// </summary>
/// 
    public List<GameObject> prefabsToPaintAllLocationsDestruction = new List<GameObject>();
    public List<bool> prefabsToPaintStatusAllLocationsDestruction = new List<bool>();

    public List<GameObject> prefabsToPaintLocation1Destruction = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation1Destruction = new List<bool>();

    public List<GameObject> prefabsToPaintLocation2Destruction = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation2Destruction = new List<bool>();

    public List<GameObject> prefabsToPaintLocation3Destruction = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation3Destruction = new List<bool>();

    public List<GameObject> prefabsToPaintLocation4Destruction = new List<GameObject>();
    public List<bool> prefabsToPaintStatusLocation4Destruction = new List<bool>();

    public List<GameObject> prefabsToPaint = new List<GameObject>();

    public List<GameObject> chanks = new List<GameObject>();
    public List<bool> chanksStatus = new List<bool>();

    public List<Material> chanksMaterial = new List<Material>();
    public List<bool> chanksMaterialStatus = new List<bool>();

    public List<GameObject> chanksToSpawn = new List<GameObject>();
    public List<Material> chanksMaterialToUse = new List<Material>();

    [System.Serializable]
    public class SpawnHistory
    {
        public List<GameObject> spawnedObjects = new List<GameObject>();
        public List<GameObject> objectRef = new List<GameObject>();
        public List<Vector3> position = new List<Vector3>();
        public List<Quaternion> rotation = new List<Quaternion>();
        public List<Transform> parent = new List<Transform>();
        public int mouseClickIndex = -1;
        public int actionIndex = -1;//0-spawn,1-delete
        public List<int> instanceID = new List<int>();
    }

    [System.Serializable]
    public class DeleteHistory
    {
        //public List<GameObject> deletedObjects = new List<GameObject>();
        public List<GameObject> objectRef = new List<GameObject>();
        public List<Vector3> position = new List<Vector3>();
        public List<Quaternion> rotation = new List<Quaternion>();
        public List<Transform> parent = new List<Transform>();
        public int mouseClickIndex = -1;
        public int actionIndex = -1;//0-spawn,1-delete
        public List<int> instanceID = new List<int>();
    }

    public List<SpawnHistory> historySpawn = new List<SpawnHistory>();
    public List<DeleteHistory> historyDelete = new List<DeleteHistory>();

    public int currentObjectToSpawnIndex = 0;
    public float currentRotateByY = 0;

    public GameObject currentObjectPreview;
    public GameObject currentHittedObject;

    public GameObject[] objectsToDelete;

    public float rateSave = 5.0f;
    public float timerSave = 0.0f;

    MapEditorSettings myInstance;

    public Quaternion GetRotation(Vector3 position, Vector3 surface_norm)
    {
        Quaternion surface_rot = Quaternion.identity;
        float[] rand_euler = { 0.0f, 0.0f, 0.0f };

        surface_rot = Quaternion.AngleAxis(Vector3.Angle(Vector3.up, surface_norm), Vector3.Cross(Vector3.up, surface_norm));
        return surface_rot;
    }

    public bool HasPrefabs()
    {
        if (prefabsToPaint.Count > 0 && prefabsToPaint != null)
            return true;

        return false;
    }

    [MenuItem("Редакторы/Редактор карт")]
    static void Init()
    {
        MapEditor window = (MapEditor)EditorWindow.GetWindow(typeof(MapEditor));
        window.title = "Редактор Карт :)";
        window.Show();
    }

    void OnSelectionChange()
    {
        EditorWindow window = EditorWindow.GetWindow<MapEditor>();

        window.Repaint();
    }

    void OnEnable()
    {
        showPaint = new AnimBool(true);
        showPaint.valueChanged.AddListener(Repaint);
        showObjectsToSpawn = new AnimBool(true);
        showObjectsToSpawn.valueChanged.AddListener(Repaint);
        showChanksSettings = new AnimBool(true);
        showChanksSettings.valueChanged.AddListener(Repaint);


        myInstance = (MapEditorSettings)AssetDatabase.LoadAssetAtPath("Assets/Resources/MapEditor/Settings/Settings.asset", typeof(MapEditorSettings)) as MapEditorSettings;
        if (myInstance == null)
        {
            //Debug.Log("No");
            myInstance = CreateInstance<MapEditorSettings>();
            myInstance.brushColor = brushColor;
            myInstance.brushSize = size;
            myInstance.maxBrushSize = sizeMax;
            myInstance.fillMode = fillMode;
            myInstance.fillModeRate = rate;
            myInstance.modeIndex = modeIndex;

            myInstance.brushLayer = brushLayer;
            myInstance.brushMode = brushMode;

            myInstance.randomRotatedChanks = randomRotatedChanks;
            myInstance.randomRotatedSpawnObjects = randomRotatedSpawnObjects;
            myInstance.rotateValues = rotateValues;
            myInstance.megaChankSize = megaChankSize;

            myInstance.historySpawn = historySpawn;
            myInstance.historyDelete = historyDelete;

            ReloadPrefabs();
            ReloadChanks();

            myInstance.prefabsToPaintAllLocations = prefabsToPaintAllLocations;
            myInstance.prefabsToPaintStatusAllLocations = prefabsToPaintStatusAllLocations;

            myInstance.prefabsToPaintLocation1 = prefabsToPaintLocation1;
            myInstance.prefabsToPaintStatusLocation1 = prefabsToPaintStatusLocation1;

            myInstance.prefabsToPaintLocation2 = prefabsToPaintLocation2;
            myInstance.prefabsToPaintStatusLocation2 = prefabsToPaintStatusLocation2;

            myInstance.prefabsToPaintLocation3 = prefabsToPaintLocation3;
            myInstance.prefabsToPaintStatusLocation3 = prefabsToPaintStatusLocation3;

            myInstance.prefabsToPaintLocation4 = prefabsToPaintLocation4;
            myInstance.prefabsToPaintStatusLocation4 = prefabsToPaintStatusLocation4;

            myInstance.prefabsToPaintAllLocationsDestruction = prefabsToPaintAllLocationsDestruction;
            myInstance.prefabsToPaintStatusAllLocationsDestruction = prefabsToPaintStatusAllLocationsDestruction;

            myInstance.prefabsToPaintLocation1Destruction = prefabsToPaintLocation1Destruction;
            myInstance.prefabsToPaintStatusLocation1Destruction = prefabsToPaintStatusLocation1Destruction;

            myInstance.prefabsToPaintLocation2Destruction = prefabsToPaintLocation2Destruction;
            myInstance.prefabsToPaintStatusLocation2Destruction = prefabsToPaintStatusLocation2Destruction;

            myInstance.prefabsToPaintLocation3Destruction = prefabsToPaintLocation3Destruction;
            myInstance.prefabsToPaintStatusLocation3Destruction = prefabsToPaintStatusLocation3Destruction;

            myInstance.prefabsToPaintLocation4Destruction = prefabsToPaintLocation4Destruction;
            myInstance.prefabsToPaintStatusLocation4Destruction = prefabsToPaintStatusLocation4Destruction;

            myInstance.chanksMaterial = chanksMaterial;
            myInstance.chanksMaterialStatus = chanksMaterialStatus;

            myInstance.chanks = chanks;
            myInstance.chanksStatus = chanksStatus;

            ReloadObjectsToSpawn();
            ReloadChanksToSpawn();

            AssetDatabase.CreateAsset(myInstance, "Assets/Resources/MapEditor/Settings/Settings.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }else
        {
            //Debug.Log("Yes");
            brushColor = myInstance.brushColor;
            size = myInstance.brushSize;
            sizeMax = myInstance.maxBrushSize;
            fillMode = myInstance.fillMode;
            rate = myInstance.fillModeRate;

            brushLayer = myInstance.brushLayer;
            brushMode = myInstance.brushMode;

            modeIndex = myInstance.modeIndex;

            randomRotatedChanks = myInstance.randomRotatedChanks;
            randomRotatedSpawnObjects = myInstance.randomRotatedSpawnObjects;
            rotateValues = myInstance.rotateValues;
            megaChankSize = myInstance.megaChankSize;

            historySpawn = myInstance.historySpawn;
            historyDelete = myInstance.historyDelete;

            prefabsToPaintAllLocations = myInstance.prefabsToPaintAllLocations;
            prefabsToPaintStatusAllLocations = myInstance.prefabsToPaintStatusAllLocations;

            prefabsToPaintLocation1 = myInstance.prefabsToPaintLocation1;
            prefabsToPaintStatusLocation1 = myInstance.prefabsToPaintStatusLocation1;

            prefabsToPaintLocation2 = myInstance.prefabsToPaintLocation2;
            prefabsToPaintStatusLocation2 = myInstance.prefabsToPaintStatusLocation2;

            prefabsToPaintLocation3 = myInstance.prefabsToPaintLocation3;
            prefabsToPaintStatusLocation3 = myInstance.prefabsToPaintStatusLocation3;

            prefabsToPaintLocation4 = myInstance.prefabsToPaintLocation4;
            prefabsToPaintStatusLocation4 = myInstance.prefabsToPaintStatusLocation4;


            prefabsToPaintAllLocationsDestruction = myInstance.prefabsToPaintAllLocationsDestruction;
            prefabsToPaintStatusAllLocationsDestruction = myInstance.prefabsToPaintStatusAllLocationsDestruction;

            prefabsToPaintLocation1Destruction = myInstance.prefabsToPaintLocation1Destruction;
            prefabsToPaintStatusLocation1Destruction = myInstance.prefabsToPaintStatusLocation1Destruction;

            prefabsToPaintLocation2Destruction = myInstance.prefabsToPaintLocation2Destruction;
            prefabsToPaintStatusLocation2Destruction = myInstance.prefabsToPaintStatusLocation2Destruction;

            prefabsToPaintLocation3Destruction = myInstance.prefabsToPaintLocation3Destruction;
            prefabsToPaintStatusLocation3Destruction = myInstance.prefabsToPaintStatusLocation3Destruction;

            prefabsToPaintLocation4Destruction = myInstance.prefabsToPaintLocation4Destruction;
            prefabsToPaintStatusLocation4Destruction = myInstance.prefabsToPaintStatusLocation4Destruction;

            chanksMaterial = myInstance.chanksMaterial;
            chanksMaterialStatus = myInstance.chanksMaterialStatus;

            chanks = myInstance.chanks;
            chanksStatus = myInstance.chanksStatus;

            ReloadObjectsToSpawn();
            ReloadChanksToSpawn();
        }


    }

    void OnInspectorUpdate()
    {
        this.Repaint();
        ReloadObjectsToSpawn();
        ReloadChanksToSpawn();

        if (Time.realtimeSinceStartup - timerSave >= rateSave)
            SaveData();
    }

    void ReloadChanks()
    {
        chanks.Clear();
        chanksMaterial.Clear();
        GameObject[] ch = Resources.LoadAll<GameObject>("MapEditor/Chanks");
        bool[] chB = new bool[ch.Length];
        chanks.AddRange(ch);
        chanksStatus.AddRange(chB);

        Material[] chm = Resources.LoadAll<Material>("MapEditor/Chanks/Materials");
        bool[] chmB = new bool[chm.Length];
        chanksMaterial.AddRange(chm);
        chanksMaterialStatus.AddRange(chmB);

        ReloadChanksToSpawn();
    }

    void ReloadPrefabs()
    {
        Debug.Log("Reload!");

        prefabsToPaintAllLocations.Clear();
        prefabsToPaintLocation1.Clear();
        prefabsToPaintLocation2.Clear();
        prefabsToPaintLocation3.Clear();
        prefabsToPaintLocation4.Clear();

        prefabsToPaintStatusAllLocations.Clear();
        prefabsToPaintStatusLocation1.Clear();
        prefabsToPaintStatusLocation2.Clear();
        prefabsToPaintStatusLocation3.Clear();
        prefabsToPaintStatusLocation4.Clear();

        prefabsToPaintAllLocationsDestruction.Clear();
        prefabsToPaintLocation1Destruction.Clear();
        prefabsToPaintLocation2Destruction.Clear();
        prefabsToPaintLocation3Destruction.Clear();
        prefabsToPaintLocation4Destruction.Clear();

        prefabsToPaintStatusAllLocationsDestruction.Clear();
        prefabsToPaintStatusLocation1Destruction.Clear();
        prefabsToPaintStatusLocation2Destruction.Clear();
        prefabsToPaintStatusLocation3Destruction.Clear();
        prefabsToPaintStatusLocation4Destruction.Clear();

        GameObject[] all = Resources.LoadAll<GameObject>("MapEditor/Objects/DecorationObjects/AllLocations");
        bool[] allB = new bool[all.Length];
        prefabsToPaintAllLocations.AddRange(all);
        prefabsToPaintStatusAllLocations.AddRange(allB);

        GameObject[] location1 = Resources.LoadAll<GameObject>("MapEditor/Objects/DecorationObjects/Desert");
        bool[] location1B = new bool[location1.Length];
        prefabsToPaintLocation1.AddRange(location1);
        prefabsToPaintStatusLocation1.AddRange(location1B);

        GameObject[] location2 = Resources.LoadAll<GameObject>("MapEditor/Objects/DecorationObjects/Swamp");
        bool[] location2B = new bool[location2.Length];
        prefabsToPaintLocation2.AddRange(location2);
        prefabsToPaintStatusLocation2.AddRange(location2B);

        GameObject[] location3 = Resources.LoadAll<GameObject>("MapEditor/Objects/DecorationObjects/OilCity");
        bool[] location3B = new bool[location3.Length];
        prefabsToPaintLocation3.AddRange(location3);
        prefabsToPaintStatusLocation3.AddRange(location3B);

        GameObject[] location4 = Resources.LoadAll<GameObject>("MapEditor/Objects/DecorationObjects/City");
        bool[] location4B = new bool[location4.Length];
        prefabsToPaintLocation4.AddRange(location4);
        prefabsToPaintStatusLocation4.AddRange(location4B);




        GameObject[] allDestruction = Resources.LoadAll<GameObject>("MapEditor/Objects/DestructionObjects/AllLocations");
        bool[] allDestructionB = new bool[allDestruction.Length];
        prefabsToPaintAllLocationsDestruction.AddRange(allDestruction);
        prefabsToPaintStatusAllLocationsDestruction.AddRange(allDestructionB);

        GameObject[] location1Destruction = Resources.LoadAll<GameObject>("MapEditor/Objects/DestructionObjects/Desert");
        bool[] location1DestructionB = new bool[location1Destruction.Length];
        prefabsToPaintLocation1Destruction.AddRange(location1Destruction);
        prefabsToPaintStatusLocation1Destruction.AddRange(location1DestructionB);

        GameObject[] location2Destruction = Resources.LoadAll<GameObject>("MapEditor/Objects/DestructionObjects/Swamp");
        bool[] location2DestructionB = new bool[location2Destruction.Length];
        prefabsToPaintLocation2Destruction.AddRange(location2Destruction);
        prefabsToPaintStatusLocation2Destruction.AddRange(location2DestructionB);

        GameObject[] location3Destruction = Resources.LoadAll<GameObject>("MapEditor/Objects/DestructionObjects/OilCity");
        bool[] location3DestructionB = new bool[location3Destruction.Length];
        prefabsToPaintLocation3Destruction.AddRange(location3Destruction);
        prefabsToPaintStatusLocation3Destruction.AddRange(location3DestructionB);

        GameObject[] location4Destruction = Resources.LoadAll<GameObject>("MapEditor/Objects/DestructionObjects/City");
        bool[] location4DestructionB = new bool[location4Destruction.Length];
        prefabsToPaintLocation4Destruction.AddRange(location4Destruction);
        prefabsToPaintStatusLocation4Destruction.AddRange(location4DestructionB);

        ReloadObjectsToSpawn();

    }

    bool showPosition = false;

    bool showObjectsListAllLocations = false;
    bool showObjectsListLocation1 = false;
    bool showObjectsListLocation2 = false;
    bool showObjectsListLocation3 = false;
    bool showObjectsListLocation4 = false;


    bool showObjectsListAllLocationsDestruction = false;
    bool showObjectsListLocation1Destruction = false;
    bool showObjectsListLocation2Destruction = false;
    bool showObjectsListLocation3Destruction = false;
    bool showObjectsListLocation4Destruction = false;

    bool showChanksList = false;
    bool showChanksMaterialsList = false;


    void ObjectsListGUILocations(int index)
    {

        string curName = "";
        bool showList = false;
        List<GameObject> currentObjects = new List<GameObject>();
        List<bool> currentObjectsStatus = new List<bool>();

        if (index == 0)
        {
            curName = "всех локаций";
            currentObjects = prefabsToPaintAllLocations;
            currentObjectsStatus = prefabsToPaintStatusAllLocations;
        }
        if (index == 1)
        {
            curName = "пустыни";
            currentObjects = prefabsToPaintLocation1;
            currentObjectsStatus = prefabsToPaintStatusLocation1;
        }
        if (index == 2)
        {
            curName = "болота";
            currentObjects = prefabsToPaintLocation2;
            currentObjectsStatus = prefabsToPaintStatusLocation2;
        }
        if (index == 3)
        {
            curName = "нефтегорода";
            currentObjects = prefabsToPaintLocation3;
            currentObjectsStatus = prefabsToPaintStatusLocation3;
        }
        if (index == 4)
        {
            curName = "города";
            currentObjects = prefabsToPaintLocation4;
            currentObjectsStatus = prefabsToPaintStatusLocation4;
        }

        if (index == 5)
        {
            curName = "всех локаций";
            currentObjects = prefabsToPaintAllLocationsDestruction;
            currentObjectsStatus = prefabsToPaintStatusAllLocationsDestruction;
        }
        if (index == 6)
        {
            curName = "пустыни";
            currentObjects = prefabsToPaintLocation1Destruction;
            currentObjectsStatus = prefabsToPaintStatusLocation1Destruction;
        }
        if (index == 7)
        {
            curName = "болота";
            currentObjects = prefabsToPaintLocation2Destruction;
            currentObjectsStatus = prefabsToPaintStatusLocation2Destruction;
        }
        if (index == 8)
        {
            curName = "нефтегорода";
            currentObjects = prefabsToPaintLocation3Destruction;
            currentObjectsStatus = prefabsToPaintStatusLocation3Destruction;
        }
        if (index == 9)
        {
            curName = "города";
            currentObjects = prefabsToPaintLocation4Destruction;
            currentObjectsStatus = prefabsToPaintStatusLocation4Destruction;
        }
        if(index<5)
        GUILayout.Label("  Декоративные Объекты для "+ curName, EditorStyles.boldLabel);
        else
        GUILayout.Label("   Разрушаемые Объекты для " + curName, EditorStyles.boldLabel);

        string status = "";

        if (index == 0)
        {
            status = showObjectsListAllLocations ? "   Спрятать" : "   Показать";
            showObjectsListAllLocations = EditorGUILayout.Foldout(showObjectsListAllLocations, status);
            showList = showObjectsListAllLocations;
        }
        if (index == 1)
        {
            status = showObjectsListLocation1 ? "   Спрятать" : "   Показать";
            showObjectsListLocation1 = EditorGUILayout.Foldout(showObjectsListLocation1, status);
            showList = showObjectsListLocation1;
        }
        if (index == 2)
        {
            status = showObjectsListLocation2 ? "   Спрятать" : "   Показать";
            showObjectsListLocation2 = EditorGUILayout.Foldout(showObjectsListLocation2, status);
            showList = showObjectsListLocation2;
        }
        if (index == 3)
        {
            status = showObjectsListLocation3 ? "   Спрятать" : "   Показать";
            showObjectsListLocation3 = EditorGUILayout.Foldout(showObjectsListLocation3, status);
            showList = showObjectsListLocation3;
        }
        if (index == 4)
        {
            status = showObjectsListLocation4 ? "   Спрятать" : "   Показать";
            showObjectsListLocation4 = EditorGUILayout.Foldout(showObjectsListLocation4, status);
            showList = showObjectsListLocation4;
        }

        if (index == 5)
        {
            status = showObjectsListAllLocationsDestruction ? "   Спрятать" : "   Показать";
            showObjectsListAllLocationsDestruction = EditorGUILayout.Foldout(showObjectsListAllLocationsDestruction, status);
            showList = showObjectsListAllLocationsDestruction;
        }
        if (index == 6)
        {
            status = showObjectsListLocation1Destruction ? "   Спрятать" : "   Показать";
            showObjectsListLocation1Destruction = EditorGUILayout.Foldout(showObjectsListLocation1Destruction, status);
            showList = showObjectsListLocation1Destruction;
        }
        if (index == 7)
        {
            status = showObjectsListLocation2Destruction ? "   Спрятать" : "   Показать";
            showObjectsListLocation2Destruction = EditorGUILayout.Foldout(showObjectsListLocation2Destruction, status);
            showList = showObjectsListLocation2Destruction;
        }
        if (index == 8)
        {
            status = showObjectsListLocation3Destruction ? "   Спрятать" : "   Показать";
            showObjectsListLocation3Destruction = EditorGUILayout.Foldout(showObjectsListLocation3Destruction, status);
            showList = showObjectsListLocation3Destruction;
        }
        if (index == 9)
        {
            status = showObjectsListLocation4Destruction ? "   Спрятать" : "   Показать";
            showObjectsListLocation4Destruction = EditorGUILayout.Foldout(showObjectsListLocation4Destruction, status);
            showList = showObjectsListLocation4Destruction;
        }

        //status = showObjectsListAllLocations ? "Спрятать" : "Показать";

        //showObjectsListAllLocations = EditorGUILayout.Foldout(showObjectsListAllLocations, status);

        float alpha = 1;
        if (myInstance)
            alpha = myInstance.previewObjectsImageZoom;

        float lerp = Mathf.Lerp(30, 64, alpha);

        if (showList)
        {
            int n = currentObjects.Count;
            float n2 = Mathf.Sqrt(n);
            int n3 = Mathf.FloorToInt(n2) + 1;
            if (alpha <= 0)
            {
                    EditorGUILayout.BeginVertical();
                    for (int i = 0; i < n; i++)
                    {
                    if (index < 5)
                    {
                        if (myInstance != null)
                        {
                            GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                            if (currentObjectsStatus[i])
                                GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                        }
                        else
                        {
                            GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                            if (currentObjectsStatus[i])
                                GUI.backgroundColor = Color.green;
                        }
                    }else
                    {
                        if (myInstance != null)
                        {
                            GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnDestructionColor;
                            if (currentObjectsStatus[i])
                                GUI.backgroundColor = myInstance.selectedObjectToSpawnDestructionColor;
                        }
                        else
                        {
                            GUI.backgroundColor = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.2f);
                            if (currentObjectsStatus[i])
                                GUI.backgroundColor = Color.yellow;
                        }
                    }

                            if (GUILayout.Button(currentObjects[i].name, GUILayout.Width(Screen.width), GUILayout.Height(16)))
                                currentObjectsStatus[i] = !currentObjectsStatus[i];
                            GUI.backgroundColor = Color.white;

                    }
                    EditorGUILayout.EndVertical();
            }else
            {
                EditorGUILayout.BeginHorizontal();
                for (int x = 0; x < n3; x++)
                {
                    EditorGUILayout.BeginVertical();
                    for (int y = 0; y < n3; y++)
                    {
                        if ((x + y * n3) < n)
                        {
                            int i = x + y * n3;

                            if (index < 5)
                            {
                                if (myInstance != null)
                                {
                                    GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                                    if (currentObjectsStatus[i])
                                        GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                                }
                                else
                                {
                                    GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                                    if (currentObjectsStatus[i])
                                        GUI.backgroundColor = Color.green;
                                }
                            }
                            else
                            {
                                if (myInstance != null)
                                {
                                    GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnDestructionColor;
                                    if (currentObjectsStatus[i])
                                        GUI.backgroundColor = myInstance.selectedObjectToSpawnDestructionColor;
                                }
                                else
                                {
                                    GUI.backgroundColor = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.2f);
                                    if (currentObjectsStatus[i])
                                        GUI.backgroundColor = Color.yellow;
                                }
                            }

                            Texture2D icon = AssetPreview.GetAssetPreview(currentObjects[i]);
                            if (GUILayout.Button(icon, GUILayout.Width(lerp), GUILayout.Height(lerp)))
                                currentObjectsStatus[i] = !currentObjectsStatus[i];
                            GUI.backgroundColor = Color.white;
                            if (myInstance && myInstance.showSpawnObjectsNames)
                                GUILayout.Label(currentObjects[i].name, EditorStyles.boldLabel);
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
        }
    }

    void ObjectsListGUIChanks()
    {

        GUILayout.Label("   Чанки и их материалы", EditorStyles.boldLabel);
        string status = "";
        string statusM = "";

        status = showChanksList ? "   Спрятать чанки" : "   Показать чанки";
        showChanksList = EditorGUILayout.Foldout(showChanksList, status);


        float alpha = 1;
        if (myInstance)
            alpha = myInstance.previewObjectsImageZoom;

        float lerp = Mathf.Lerp(30, 64, alpha);

        if (showChanksList)
        {
            int n = chanks.Count;
            float n2 = Mathf.Sqrt(n);
            int n3 = Mathf.FloorToInt(n2) + 1;
            if (alpha <= 0)
            {
                EditorGUILayout.BeginVertical();
                for (int i = 0; i < n; i++)
                {
                    if (myInstance != null)
                    {
                        GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                        if (chanksStatus[i])
                            GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                    }
                    else
                    {
                        GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                        if (chanksStatus[i])
                            GUI.backgroundColor = Color.green;
                    }

                    if (GUILayout.Button(chanks[i].name, GUILayout.Width(Screen.width), GUILayout.Height(16)))
                        chanksStatus[i] = !chanksStatus[i];
                    GUI.backgroundColor = Color.white;
                }
                EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                for (int x = 0; x < n3; x++)
                {
                    EditorGUILayout.BeginVertical();
                    for (int y = 0; y < n3; y++)
                    {
                        if ((x + y * n3) < n)
                        {
                            int i = x + y * n3;
                            if (myInstance != null)
                            {
                                GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                                if (chanksStatus[i])
                                    GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                            }
                            else
                            {
                                GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                                if (chanksStatus[i])
                                    GUI.backgroundColor = Color.green;
                            }
                            Texture2D icon = AssetPreview.GetAssetPreview(chanks[i]);
                            if (GUILayout.Button(icon, GUILayout.Width(lerp), GUILayout.Height(lerp)))
                                chanksStatus[i] = !chanksStatus[i];
                            GUI.backgroundColor = Color.white;
                            if (myInstance && myInstance.showChanksAndMaterialsNames)
                                GUILayout.Label(chanks[i].name, EditorStyles.boldLabel);
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        statusM = showChanksMaterialsList ? "   Спрятать материалы" : "   Показать материалы";
        showChanksMaterialsList = EditorGUILayout.Foldout(showChanksMaterialsList, statusM);

        if (showChanksMaterialsList)
        {
            int n = chanksMaterial.Count;
            float n2 = Mathf.Sqrt(n);
            int n3 = Mathf.FloorToInt(n2) + 1;
            if (alpha <= 0)
            {
                EditorGUILayout.BeginVertical();
                for (int i = 0; i < n; i++)
                {
                    if (myInstance != null)
                    {
                        GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                        if (chanksMaterialStatus[i])
                            GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                    }
                    else
                    {
                        GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                        if (chanksMaterialStatus[i])
                            GUI.backgroundColor = Color.green;
                    }

                    if (GUILayout.Button(chanksMaterial[i].name, GUILayout.Width(Screen.width), GUILayout.Height(16)))
                        chanksMaterialStatus[i] = !chanksMaterialStatus[i];
                    GUI.backgroundColor = Color.white;
                }
                EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                for (int x = 0; x < n3; x++)
                {
                    EditorGUILayout.BeginVertical();
                    for (int y = 0; y < n3; y++)
                    {
                        if ((x + y * n3) < n)
                        {
                            int i = x + y * n3;
                            if (myInstance != null)
                            {
                                GUI.backgroundColor = myInstance.nonSelectedObjectToSpawnColor;
                                if (chanksMaterialStatus[i])
                                    GUI.backgroundColor = myInstance.selectedObjectToSpawnColor;
                            }
                            else
                            {
                                GUI.backgroundColor = new Color(Color.red.r, Color.red.g, Color.red.b, 0.2f);
                                if (chanksMaterialStatus[i])
                                    GUI.backgroundColor = Color.green;
                            }
                            Texture2D icon = AssetPreview.GetAssetPreview(chanksMaterial[i]);
                            if (GUILayout.Button(icon, GUILayout.Width(lerp), GUILayout.Height(lerp)))
                                chanksMaterialStatus[i] = !chanksMaterialStatus[i];
                            GUI.backgroundColor = Color.white;
                            if (myInstance && myInstance.showChanksAndMaterialsNames)
                                GUILayout.Label(chanksMaterial[i].name, EditorStyles.boldLabel);
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
        }
    }

    void ReloadObjectsToSpawn()
    {
        prefabsToPaint.Clear();
        //Do 
        for (int i = 0; i < prefabsToPaintStatusAllLocations.Count; i++)
        {
            if (prefabsToPaintStatusAllLocations[i])
            {
                prefabsToPaint.Add(prefabsToPaintAllLocations[i]);
            }
        }
        for (int i1 = 0; i1 < prefabsToPaintStatusLocation1.Count; i1++)
        {
            if (prefabsToPaintStatusLocation1[i1])
            {
                prefabsToPaint.Add(prefabsToPaintLocation1[i1]);
            }
        }
        for (int i2 = 0; i2 < prefabsToPaintStatusLocation2.Count; i2++)
        {
            if (prefabsToPaintStatusLocation2[i2])
            {
                prefabsToPaint.Add(prefabsToPaintLocation2[i2]);
            }
        }
        for (int i3 = 0; i3 < prefabsToPaintStatusLocation3.Count; i3++)
        {
            if (prefabsToPaintStatusLocation3[i3])
            {
                prefabsToPaint.Add(prefabsToPaintLocation3[i3]);
            }
        }
        for (int i4 = 0; i4 < prefabsToPaintStatusLocation4.Count; i4++)
        {
            if (prefabsToPaintStatusLocation4[i4])
            {
                prefabsToPaint.Add(prefabsToPaintLocation4[i4]);
            }
        }


        for (int iDestruction = 0; iDestruction < prefabsToPaintStatusAllLocationsDestruction.Count; iDestruction++)
        {
            if (prefabsToPaintStatusAllLocationsDestruction[iDestruction])
            {
                prefabsToPaint.Add(prefabsToPaintAllLocationsDestruction[iDestruction]);
            }
        }
        for (int i1Destruction = 0; i1Destruction < prefabsToPaintStatusLocation1Destruction.Count; i1Destruction++)
        {
            if (prefabsToPaintStatusLocation1Destruction[i1Destruction])
            {
                prefabsToPaint.Add(prefabsToPaintLocation1Destruction[i1Destruction]);
            }
        }
        for (int i2Destruction = 0; i2Destruction < prefabsToPaintStatusLocation2Destruction.Count; i2Destruction++)
        {
            if (prefabsToPaintStatusLocation2Destruction[i2Destruction])
            {
                prefabsToPaint.Add(prefabsToPaintLocation2Destruction[i2Destruction]);
            }
        }
        for (int i3Destruction = 0; i3Destruction < prefabsToPaintStatusLocation3Destruction.Count; i3Destruction++)
        {
            if (prefabsToPaintStatusLocation3Destruction[i3Destruction])
            {
                prefabsToPaint.Add(prefabsToPaintLocation3Destruction[i3Destruction]);
            }
        }
        for (int i4Destruction = 0; i4Destruction < prefabsToPaintStatusLocation4Destruction.Count; i4Destruction++)
        {
            if (prefabsToPaintStatusLocation4Destruction[i4Destruction])
            {
                prefabsToPaint.Add(prefabsToPaintLocation4Destruction[i4Destruction]);
            }
        }
    }

    void ReloadChanksToSpawn()
    {
        chanksToSpawn.Clear();
        chanksMaterialToUse.Clear();

        for (int i = 0; i < chanksStatus.Count; i++)
        {
            if (chanksStatus[i])
            {
                chanksToSpawn.Add(chanks[i]);
            }
        }
        for (int i = 0; i < chanksMaterialStatus.Count; i++)
        {
            if (chanksMaterialStatus[i])
            {
                chanksMaterialToUse.Add(chanksMaterial[i]);
            }
        }
    }

    public void SaveData()
    {
        timerSave = Time.realtimeSinceStartup;

        if (myInstance != null)
        {

            myInstance.brushColor = brushColor;
            myInstance.brushSize = size;
            myInstance.maxBrushSize = sizeMax;
            myInstance.fillMode = fillMode;
            myInstance.fillModeRate = rate;
            myInstance.modeIndex = modeIndex;
            myInstance.brushLayer = brushLayer;
            myInstance.brushMode = brushMode;

            myInstance.randomRotatedChanks = randomRotatedChanks;
            myInstance.randomRotatedSpawnObjects = randomRotatedSpawnObjects;
            myInstance.rotateValues = rotateValues;
            myInstance.megaChankSize = megaChankSize;

            myInstance.historySpawn = historySpawn;
            myInstance.historyDelete = historyDelete;

            if (myInstance.prefabsToPaintStatusAllLocations != prefabsToPaintStatusAllLocations || myInstance.prefabsToPaintStatusLocation1 != prefabsToPaintStatusLocation1 ||
                myInstance.prefabsToPaintStatusLocation2 != prefabsToPaintStatusLocation2 || myInstance.prefabsToPaintStatusLocation3 != prefabsToPaintStatusLocation3 ||
                myInstance.prefabsToPaintStatusLocation4 != prefabsToPaintStatusLocation4 ||
                myInstance.prefabsToPaintStatusAllLocationsDestruction != prefabsToPaintStatusAllLocationsDestruction || myInstance.prefabsToPaintStatusLocation1Destruction != prefabsToPaintStatusLocation1Destruction ||
                myInstance.prefabsToPaintStatusLocation2Destruction != prefabsToPaintStatusLocation2Destruction || myInstance.prefabsToPaintStatusLocation3Destruction != prefabsToPaintStatusLocation3Destruction ||
                myInstance.prefabsToPaintStatusLocation4Destruction != prefabsToPaintStatusLocation4Destruction)
            {
                ReloadObjectsToSpawn();
            }

            if (myInstance.chanksStatus != chanksStatus || myInstance.chanksMaterialStatus != chanksMaterialStatus)
            {
                ReloadChanksToSpawn();
            }

            myInstance.prefabsToPaintAllLocations = prefabsToPaintAllLocations;
            myInstance.prefabsToPaintStatusAllLocations = prefabsToPaintStatusAllLocations;

            myInstance.prefabsToPaintLocation1 = prefabsToPaintLocation1;
            myInstance.prefabsToPaintStatusLocation1 = prefabsToPaintStatusLocation1;

            myInstance.prefabsToPaintLocation2 = prefabsToPaintLocation2;
            myInstance.prefabsToPaintStatusLocation2 = prefabsToPaintStatusLocation2;

            myInstance.prefabsToPaintLocation3 = prefabsToPaintLocation3;
            myInstance.prefabsToPaintStatusLocation3 = prefabsToPaintStatusLocation3;

            myInstance.prefabsToPaintLocation4 = prefabsToPaintLocation4;
            myInstance.prefabsToPaintStatusLocation4 = prefabsToPaintStatusLocation4;

            myInstance.prefabsToPaintAllLocationsDestruction = prefabsToPaintAllLocationsDestruction;
            myInstance.prefabsToPaintStatusAllLocationsDestruction = prefabsToPaintStatusAllLocationsDestruction;

            myInstance.prefabsToPaintLocation1Destruction = prefabsToPaintLocation1Destruction;
            myInstance.prefabsToPaintStatusLocation1Destruction = prefabsToPaintStatusLocation1Destruction;

            myInstance.prefabsToPaintLocation2Destruction = prefabsToPaintLocation2Destruction;
            myInstance.prefabsToPaintStatusLocation2Destruction = prefabsToPaintStatusLocation2Destruction;

            myInstance.prefabsToPaintLocation3Destruction = prefabsToPaintLocation3Destruction;
            myInstance.prefabsToPaintStatusLocation3Destruction = prefabsToPaintStatusLocation3Destruction;

            myInstance.prefabsToPaintLocation4Destruction = prefabsToPaintLocation4Destruction;
            myInstance.prefabsToPaintStatusLocation4Destruction = prefabsToPaintStatusLocation4Destruction;
        }
    }

    public void OnGUI()
    {
        /////////
        if(myInstance!=null)
        GUI.color = myInstance.backgroundColor;
        else
        GUI.color = new Color(0.5f, 0.5f, 0.5f, 1);
        GUI.Box(new Rect(new Vector2(0,0),new Vector2(Screen.width, Screen.height)), "");
        GUI.color = Color.white;

        if (showPaint.target)
            statusPaint = "Спрятать настройки";
        else
            statusPaint = "Показать настройки";

        if (showObjectsToSpawn.target)
            statusObjectsToSpawn = "Спрятать объекты для рисования";
        else
            statusObjectsToSpawn = "Показать объекты для рисования";

        if (showChanksSettings.target)
            statusChanksSettings = "Спрятать настройки генерации чанков";
        else
            statusChanksSettings = "Показать настройки генерации чанков";

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(Screen.width), GUILayout.Height(Screen.height - 20));
        EditorGUILayout.HelpBox("Редактор Карт", MessageType.Info);

        if (GUILayout.Button(paintMode))
        {

            if (!enablePaint && HasPrefabs())
            {
                currentObjectToSpawnIndex = Random.Range(0, prefabsToPaint.Count);
                if (randomRotatedSpawnObjects)
                    currentRotateByY = Random.Range(0, 360);
            }

            enablePaint = !enablePaint;

        }



        if (enablePaint == true)
            paintMode = "Перестать " + brushMode[modeIndex];
        else
            paintMode = "Начать " + brushMode[modeIndex];

        EditorGUI.BeginChangeCheck();


        EditorGUILayout.HelpBox("Основные настройки", MessageType.None);

        showPaint.target = EditorGUILayout.Foldout(showPaint.target, statusPaint);

        if (EditorGUILayout.BeginFadeGroup(showPaint.faded))
        {

            EditorGUILayout.LabelField("✓ Режим кисти:");
            modeIndex = EditorGUILayout.Popup(modeIndex, brushMode, GUILayout.Width(Screen.width-7.5f));
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();

            if (Screen.width <= 280)
            {
                EditorGUILayout.LabelField("✓ Слой для рисования:", GUILayout.Width(Screen.width - 130.5f));
                brushLayer = EditorGUILayout.LayerField("", brushLayer, GUILayout.Width(120));
            }
            else
            {
                EditorGUILayout.LabelField("✓ Слой для рисования:", GUILayout.Width(150));
                brushLayer = EditorGUILayout.LayerField("", brushLayer, GUILayout.Width(Screen.width-160));
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();


            EditorGUILayout.BeginHorizontal();

            if (Screen.width <= 280)
            {
                EditorGUILayout.LabelField("✓ Цвет кисти:", GUILayout.Width(Screen.width - 130.5f));
                brushColor = EditorGUILayout.ColorField("", brushColor, GUILayout.Width(120));
            }
            else
            {
                EditorGUILayout.LabelField("✓ Цвет кисти:", GUILayout.Width(150));
                brushColor = EditorGUILayout.ColorField("", brushColor, GUILayout.Width(Screen.width - 160));
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("✓ Размер кисти:");
            size = EditorGUILayout.Slider(size, 0.1f, sizeMax, GUILayout.Width(Screen.width-7.5f));
            EditorGUILayout.BeginHorizontal();

            if (Screen.width <= 280)
            {
                EditorGUILayout.LabelField("✓ Макс. размер кисти:", GUILayout.Width(Screen.width - 130.5f));
                sizeMax = EditorGUILayout.FloatField("", sizeMax, GUILayout.Width(120));
            }
            else
            {
                EditorGUILayout.LabelField("✓ Макс. размер кисти:", GUILayout.Width(150));
                sizeMax = EditorGUILayout.FloatField("", sizeMax, GUILayout.Width(Screen.width - 160));
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            fillMode = EditorGUILayout.BeginToggleGroup("Режим заливки", fillMode);

            EditorGUILayout.LabelField("✓ Частота заливки:");
            rate = EditorGUILayout.Slider(rate, 0.01f, 0.5f, GUILayout.Width(Screen.width-7.5f));
            EditorGUILayout.EndToggleGroup();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            if (Screen.width <= 350)
            {
                EditorGUILayout.LabelField("✓    Включить рандомный авто-поворот объектов?", GUILayout.Width(Screen.width - 45));
                randomRotatedSpawnObjects = EditorGUILayout.Toggle(randomRotatedSpawnObjects, GUILayout.Width(40));
            }
            else
            {
                EditorGUILayout.LabelField("✓    Включить рандомный авто-поворот объектов?", GUILayout.Width(305));
                randomRotatedSpawnObjects = EditorGUILayout.Toggle(randomRotatedSpawnObjects, GUILayout.Width(40));
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

        }



        EditorGUILayout.EndFadeGroup();

        EditorGUILayout.HelpBox("Выбор объектов для рисования", MessageType.None);

        showObjectsToSpawn.target = EditorGUILayout.Foldout(showObjectsToSpawn.target, statusObjectsToSpawn);
        if (EditorGUILayout.BeginFadeGroup(showObjectsToSpawn.faded))
        {
            for (int i = 0; i < 10; i++)
                ObjectsListGUILocations(i);

        }
        EditorGUILayout.EndFadeGroup();

        EditorGUILayout.HelpBox("Выбор чанков и материалов для генерация мегачанка", MessageType.None);

        showChanksSettings.target = EditorGUILayout.Foldout(showChanksSettings.target, statusChanksSettings);
        if (EditorGUILayout.BeginFadeGroup(showChanksSettings.faded))
        {
            ObjectsListGUIChanks();
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (Screen.width <= 300)
            {
                EditorGUILayout.LabelField("✓    Включить рандомный поворот чанков?", GUILayout.Width(Screen.width - 40));
                randomRotatedChanks = EditorGUILayout.Toggle(randomRotatedChanks, GUILayout.Width(40));
            }
            else
            {
                EditorGUILayout.LabelField("✓    Включить рандомный поворот чанков?",GUILayout.Width(260));
                randomRotatedChanks = EditorGUILayout.Toggle(randomRotatedChanks, GUILayout.Width(40));
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            if (Screen.width <= 360)
            {
                EditorGUILayout.LabelField("✓    Размер мега чанка", GUILayout.Width(Screen.width - 220));
                megaChankSize = EditorGUILayout.Vector2Field("", megaChankSize, GUILayout.Width(207.5f));
            }
            else
            {
                EditorGUILayout.LabelField("✓    Размер мега чанка", GUILayout.Width(139));
                megaChankSize = EditorGUILayout.Vector2Field("", megaChankSize,GUILayout.Width(Screen.width-150.5f));
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();


            EditorGUILayout.BeginHorizontal();
            if (Screen.width <= 380)
            {
                EditorGUILayout.LabelField("✓    Название мега чанка", GUILayout.Width(Screen.width - 220));
                megaChankName = EditorGUILayout.TextField("", megaChankName, GUILayout.Width(207.5f));
            }
            else
            {
                EditorGUILayout.LabelField("✓    Название мега чанка",GUILayout.Width(159));
                megaChankName = EditorGUILayout.TextField("", megaChankName, GUILayout.Width(Screen.width -170.5f));
            }
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("Сгенирировать мегачанк"))
                GenerateMegaChank();
        }

        EditorGUILayout.EndFadeGroup();

        if (EditorGUI.EndChangeCheck())
        {
            if (myInstance != null)
            {



                myInstance.brushColor = brushColor;
                myInstance.brushSize = size;
                myInstance.maxBrushSize = sizeMax;
                myInstance.fillMode = fillMode;
                myInstance.fillModeRate = rate;
                myInstance.modeIndex = modeIndex;
                myInstance.brushLayer = brushLayer;
                myInstance.brushMode = brushMode;

                myInstance.randomRotatedChanks = randomRotatedChanks;
                myInstance.randomRotatedSpawnObjects = randomRotatedSpawnObjects;
                myInstance.rotateValues = rotateValues;
                myInstance.megaChankSize = megaChankSize;

                myInstance.historySpawn = historySpawn;
                myInstance.historyDelete = historyDelete;

                if (myInstance.prefabsToPaintStatusAllLocations != prefabsToPaintStatusAllLocations || myInstance.prefabsToPaintStatusLocation1 != prefabsToPaintStatusLocation1 ||
                    myInstance.prefabsToPaintStatusLocation2 != prefabsToPaintStatusLocation2 || myInstance.prefabsToPaintStatusLocation3 != prefabsToPaintStatusLocation3 ||
                    myInstance.prefabsToPaintStatusLocation4 != prefabsToPaintStatusLocation4 ||
                    myInstance.prefabsToPaintStatusAllLocationsDestruction != prefabsToPaintStatusAllLocationsDestruction || myInstance.prefabsToPaintStatusLocation1Destruction != prefabsToPaintStatusLocation1Destruction ||
                    myInstance.prefabsToPaintStatusLocation2Destruction != prefabsToPaintStatusLocation2Destruction || myInstance.prefabsToPaintStatusLocation3Destruction != prefabsToPaintStatusLocation3Destruction ||
                    myInstance.prefabsToPaintStatusLocation4Destruction != prefabsToPaintStatusLocation4Destruction)
                {
                    ReloadObjectsToSpawn();
                }
                
                if (myInstance.chanksStatus != chanksStatus || myInstance.chanksMaterialStatus != chanksMaterialStatus)
                {
                    ReloadChanksToSpawn();
                }

                myInstance.prefabsToPaintAllLocations = prefabsToPaintAllLocations;
                myInstance.prefabsToPaintStatusAllLocations = prefabsToPaintStatusAllLocations;

                myInstance.prefabsToPaintLocation1 = prefabsToPaintLocation1;
                myInstance.prefabsToPaintStatusLocation1 = prefabsToPaintStatusLocation1;

                myInstance.prefabsToPaintLocation2 = prefabsToPaintLocation2;
                myInstance.prefabsToPaintStatusLocation2 = prefabsToPaintStatusLocation2;

                myInstance.prefabsToPaintLocation3 = prefabsToPaintLocation3;
                myInstance.prefabsToPaintStatusLocation3 = prefabsToPaintStatusLocation3;

                myInstance.prefabsToPaintLocation4 = prefabsToPaintLocation4;
                myInstance.prefabsToPaintStatusLocation4 = prefabsToPaintStatusLocation4;

                myInstance.prefabsToPaintAllLocationsDestruction = prefabsToPaintAllLocationsDestruction;
                myInstance.prefabsToPaintStatusAllLocationsDestruction = prefabsToPaintStatusAllLocationsDestruction;

                myInstance.prefabsToPaintLocation1Destruction = prefabsToPaintLocation1Destruction;
                myInstance.prefabsToPaintStatusLocation1Destruction = prefabsToPaintStatusLocation1Destruction;

                myInstance.prefabsToPaintLocation2Destruction = prefabsToPaintLocation2Destruction;
                myInstance.prefabsToPaintStatusLocation2Destruction = prefabsToPaintStatusLocation2Destruction;

                myInstance.prefabsToPaintLocation3Destruction = prefabsToPaintLocation3Destruction;
                myInstance.prefabsToPaintStatusLocation3Destruction = prefabsToPaintStatusLocation3Destruction;

                myInstance.prefabsToPaintLocation4Destruction = prefabsToPaintLocation4Destruction;
                myInstance.prefabsToPaintStatusLocation4Destruction = prefabsToPaintStatusLocation4Destruction;
            }
        }

        if (GUILayout.Button("Обновить объекты для рисования", GUILayout.Width(Screen.width - 7.5f)))
            ReloadPrefabs();
        if (GUILayout.Button("Обновить чанки и материалы", GUILayout.Width(Screen.width - 7.5f)))
            ReloadChanks();

        EditorGUILayout.HelpBox("Версия 1.0.0f1", MessageType.Warning);
        EditorGUILayout.EndScrollView();
    }

    void GenerateMegaChank()
    {
        if (chanksToSpawn.Count == 0)
        {
            EditorUtility.DisplayDialog("Забыли выбрать чанк?",
            "Выберите хотя бы один чанк для генерации. Затем снова нажмите 'Сгенерировать мегачанк'.", "Понял", "");
            return;
        }

        GameObject chanksParent = new GameObject(megaChankName);

        float chankSize = 140f;

        float n = megaChankSize.x * megaChankSize.y;
        float n2 = Mathf.Sqrt(n);
        int n3 = Mathf.FloorToInt(n2) + 1;

        for (int i = 0; i < megaChankSize.x; i++)
            for (int j = 0; j < megaChankSize.y; j++)
            {
                int rnd = Random.Range(0, chanksToSpawn.Count);
                int rndM = Random.Range(0, chanksMaterialToUse.Count);
                int rndR = Random.Range(0, rotateValues.Length);

                GameObject chank = Instantiate(chanksToSpawn[rnd]) as GameObject;

                chank.name = chanksParent.name + " Chank " + (i + j * n3).ToString();
                chank.transform.parent = chanksParent.transform;
                chank.transform.position = new Vector3(chankSize * i, 0, chankSize * j);
                if(randomRotatedChanks)
                chank.transform.rotation *= Quaternion.Euler(0, rotateValues[rndR], 0);
                GameObject fieldGO = GameObject.Find(chank.name+"/Field");
                if(fieldGO && fieldGO.GetComponent<MeshRenderer>() && chanksMaterialToUse.Count != 0)
                fieldGO.GetComponent<MeshRenderer>().sharedMaterial = chanksMaterialToUse[rndM];
            }
    }

    void OnFocus()
    {

            // Remove if already present and register the function
            SceneView.onSceneGUIDelegate -= this.OnSceneGUI;

            // This allows us to register an update function for the scene view port
            SceneView.onSceneGUIDelegate += this.OnSceneGUI;

            // Storing our editors hash ID for control ID purposes
            _editorHash = GetHashCode();
        
    }

    void OnDestroy()
    {
        if (myInstance != null)
        {
            myInstance.brushColor = brushColor;
            myInstance.brushSize = size;
            myInstance.maxBrushSize = sizeMax;
            myInstance.fillMode = fillMode;
            myInstance.fillModeRate = rate;
            myInstance.historySpawn = historySpawn;
            myInstance.historyDelete = historyDelete;

            myInstance.brushLayer = brushLayer;
            myInstance.brushMode = brushMode;

            myInstance.modeIndex = modeIndex;

            myInstance.randomRotatedChanks = randomRotatedChanks;
            myInstance.randomRotatedSpawnObjects = randomRotatedSpawnObjects;
            myInstance.rotateValues = rotateValues;
            myInstance.megaChankSize = megaChankSize;

            myInstance.prefabsToPaintAllLocations = prefabsToPaintAllLocations;
            myInstance.prefabsToPaintStatusAllLocations = prefabsToPaintStatusAllLocations;

            myInstance.prefabsToPaintLocation1 = prefabsToPaintLocation1;
            myInstance.prefabsToPaintStatusLocation1 = prefabsToPaintStatusLocation1;

            myInstance.prefabsToPaintLocation2 = prefabsToPaintLocation2;
            myInstance.prefabsToPaintStatusLocation2 = prefabsToPaintStatusLocation2;

            myInstance.prefabsToPaintLocation3 = prefabsToPaintLocation3;
            myInstance.prefabsToPaintStatusLocation3 = prefabsToPaintStatusLocation3;

            myInstance.prefabsToPaintLocation4 = prefabsToPaintLocation4;
            myInstance.prefabsToPaintStatusLocation4 = prefabsToPaintStatusLocation4;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        // Unregister our scene update function
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;

    }

    // Updates whenever the scene is interacted with
    void OnSceneGUI(SceneView scene_view)
    {

        bool Undo;

        if (myInstance)
            Undo = Event.current.alt && (Event.current.keyCode == myInstance.keyToUndo && Event.current.type == EventType.KeyDown);
        else
            Undo = Event.current.alt && (Event.current.keyCode == KeyCode.Z && Event.current.type == EventType.KeyDown);

        bool Redo;

        if (myInstance)
            Redo = Event.current.alt && (Event.current.keyCode == myInstance.keyToRedo && Event.current.type == EventType.KeyDown);
        else
            Redo = Event.current.alt && (Event.current.keyCode == KeyCode.Y && Event.current.type == EventType.KeyDown);

        if (Undo)
        {
            Debug.Log("Undo");
            if (historySpawn.Count > 0)
            {

                DeleteHistory dh = new DeleteHistory();
                dh.actionIndex = 2;
                dh.instanceID = historySpawn[historySpawn.Count - 1].instanceID;
                dh.mouseClickIndex = historySpawn[historySpawn.Count - 1].mouseClickIndex;
                dh.objectRef = historySpawn[historySpawn.Count - 1].objectRef;
                dh.position = historySpawn[historySpawn.Count - 1].position;
                dh.rotation = historySpawn[historySpawn.Count - 1].rotation;
                dh.parent = historySpawn[historySpawn.Count - 1].parent;

                for (int i = 0; i < historySpawn[historySpawn.Count - 1].spawnedObjects.Count; i++)
                {
                    if (historySpawn[historySpawn.Count - 1].actionIndex == 1)
                    {
                        DestroyImmediate(EditorUtility.InstanceIDToObject(historySpawn[historySpawn.Count - 1].instanceID[i]));
                    }
                }
                historyDelete.Add(dh);
                historySpawn.Remove(historySpawn[historySpawn.Count - 1]);
            }
        }

        if (Redo)
        {
            Debug.Log("Redo");
            if (historyDelete.Count > 0)
            {

                SpawnHistory sh = new SpawnHistory();
                sh.actionIndex = 1;
                sh.instanceID = historyDelete[historyDelete.Count - 1].instanceID;
                sh.mouseClickIndex = historyDelete[historyDelete.Count - 1].mouseClickIndex;
                sh.objectRef = historyDelete[historyDelete.Count - 1].objectRef;
                sh.position = historyDelete[historyDelete.Count - 1].position;
                sh.rotation = historyDelete[historyDelete.Count - 1].rotation;
                sh.parent = historyDelete[historyDelete.Count - 1].parent;

                for (int i = 0; i < historyDelete[historyDelete.Count - 1].objectRef.Count; i++)
                {
                    if (historyDelete[historyDelete.Count - 1].actionIndex == 2)
                    {
                        GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(historyDelete[historyDelete.Count - 1].objectRef[i]) as GameObject;
                        obj.transform.position = historyDelete[historyDelete.Count - 1].position[i];
                        obj.transform.rotation = historyDelete[historyDelete.Count - 1].rotation[i];
                        obj.transform.parent = historyDelete[historyDelete.Count - 1].parent[i];
                        sh.instanceID[i] = obj.GetInstanceID();
                        sh.spawnedObjects.Add(obj);

                    }
                }
                historySpawn.Add(sh);
                historyDelete.Remove(historyDelete[historyDelete.Count - 1]);
            }
        }

        if (!enablePaint) {
            if (currentObjectPreview)
                DestroyImmediate(currentObjectPreview);
            return;
        }else
        {
            Tools.current = Tool.None;
            Selection.activeObject = null;
            if(modeIndex == 1 && currentObjectPreview)
                DestroyImmediate(currentObjectPreview);
        }

        if (!currentObjectPreview && prefabsToPaint.Count > currentObjectToSpawnIndex && modeIndex == 0)
        {
            currentObjectPreview = PrefabUtility.InstantiatePrefab(prefabsToPaint[currentObjectToSpawnIndex]) as GameObject;
            currentObjectPreview.hideFlags = HideFlags.HideInHierarchy;
            Collider[] c = currentObjectPreview.GetComponentsInChildren<Collider>();
            for (int i = 0; i < c.Length; i++)
                c[i].enabled = false;

            Material m = (Material)AssetDatabase.LoadAssetAtPath("Assets/Resources/MapEditor/Materials/PreviewModel.mat", typeof(Material)) as Material;
            if(m && currentObjectPreview && currentObjectPreview.GetComponentInChildren<MeshRenderer>() && currentObjectPreview.GetComponentInChildren<MeshRenderer>().sharedMaterial)
            m.mainTexture = currentObjectPreview.GetComponentInChildren<MeshRenderer>().sharedMaterial.mainTexture;
            MeshRenderer[] mr = currentObjectPreview.GetComponentsInChildren<MeshRenderer>();
            for (int i2 = 0; i2 < mr.Length; i2++)
                mr[i2].sharedMaterial = m;
        }

        ////////rotates and fill

        bool plusRotate;
        bool minusRotate;
        bool changeFillMode;
        bool randomRotate;
        bool nullRotate;
        float fixedAngle;

        if (myInstance)
            fixedAngle = myInstance.fixedAngleValue;
        else
            fixedAngle = 15;

        if (myInstance)
            plusRotate = /*Event.current.alt && */(Event.current.keyCode == myInstance.keyToPlusRotate && Event.current.type == EventType.KeyDown);
        else
            plusRotate = /*Event.current.alt && */(Event.current.keyCode == KeyCode.E && Event.current.type == EventType.KeyDown);

        if (myInstance)
            minusRotate = /*Event.current.alt && */(Event.current.keyCode == myInstance.keyToMinusRotate && Event.current.type == EventType.KeyDown);
        else
            minusRotate = /*Event.current.alt && */(Event.current.keyCode == KeyCode.Q && Event.current.type == EventType.KeyDown);

        if (myInstance)
            changeFillMode = /*Event.current.alt && */(Event.current.keyCode == myInstance.keyToFillMode && Event.current.type == EventType.KeyDown);
        else
            changeFillMode = /*Event.current.alt && */(Event.current.keyCode == KeyCode.F && Event.current.type == EventType.KeyDown);

        if (myInstance)
            randomRotate = /*Event.current.alt && */(Event.current.keyCode == myInstance.keyToRandomRotate && Event.current.type == EventType.KeyDown);
        else
            randomRotate = /*Event.current.alt && */(Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyDown);

        if (myInstance)
            nullRotate = /*Event.current.alt && */(Event.current.keyCode == myInstance.keyToNullRotate && Event.current.type == EventType.KeyDown);
        else
            nullRotate = /*Event.current.alt && */(Event.current.keyCode == KeyCode.N && Event.current.type == EventType.KeyDown);


        if (plusRotate)
            currentRotateByY += fixedAngle;
        if (minusRotate)
            currentRotateByY -= fixedAngle;
        if (changeFillMode)
            fillMode = !fillMode;
        if (randomRotate)
            currentRotateByY = Random.Range(0, 360);
        if (nullRotate)
            currentRotateByY = 0;
        //////////
        
        // Convert to world space
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        RaycastHit hit;
        RaycastHit[] hit2;
        bool result;
        result = Physics.Raycast(ray, out hit, scene_view.camera.farClipPlane);

        if (modeIndex == 1)
        {
            hit2 = Physics.SphereCastAll(ray, size, scene_view.camera.farClipPlane);
            objectsToDelete = new GameObject[hit2.Length];

            for (int i = 0; i < objectsToDelete.Length; i++)
            {
                objectsToDelete[i] = hit2[i].collider.gameObject;
            }
        }

        if (result && hit.collider.gameObject.layer == brushLayer.value)
        {
            posBrush = hit.point;
            surfaceNormBrush = hit.normal;
            if (currentObjectPreview) {
                currentObjectPreview.transform.position = posBrush;
                currentObjectPreview.transform.rotation = GetRotation(posBrush, surfaceNormBrush) * Quaternion.Euler(0, currentRotateByY, 0);
            }

            DisplayBrush(scene_view.camera);

            currentHittedObject = hit.collider.gameObject;
        }
        else
        {
            if (currentObjectPreview)
                DestroyImmediate(currentObjectPreview);

            currentHittedObject = null;
        }

        HandleBrushInput(result && hit.collider && hit.collider.gameObject.layer == brushLayer.value);
    }

    //private static Texture2D s_lineTex;

    void DisplayBrush(Camera cam)
    {


        int circle_segments = 30;
        Handles.BeginGUI();

        dirBrush = Vector3.Normalize(Vector3.Cross(posBrush, posBrush + surfaceNormBrush));

        float dist = Vector3.Distance(cam.transform.position, posBrush)/200;

        Vector3 start_point = dirBrush * size;
        Vector3 previous_point = posBrush + start_point;
        Vector3 next_point;
        Handles.color = brushColor;

        for (float i = 0.0f; i < 365; i += 360.0f / circle_segments)
        {

            next_point = posBrush + Quaternion.AngleAxis(i, surfaceNormBrush) * start_point;

            Vector3[] v3 = new Vector3[2];
            v3[0] = HandleUtility.WorldToGUIPoint(previous_point);
            v3[1] = HandleUtility.WorldToGUIPoint(next_point);

            Handles.DrawAAPolyLine(1/dist, v3);

            previous_point = next_point;
        }

        Vector3[] v32 = new Vector3[2];
        v32[0] = HandleUtility.WorldToGUIPoint(posBrush);
        v32[1] = HandleUtility.WorldToGUIPoint(posBrush + surfaceNormBrush*size);

        Handles.DrawAAPolyLine(1/dist, v32);
        Handles.color = Color.white;
        if (Event.current.shift) {
            GUI.Label(new Rect(HandleUtility.WorldToGUIPoint(posBrush + Vector3.up * 2.5f), new Vector2(164, 70)), "Режим заливки: " + (fillMode ? "включен" : "выключен") + 
                "\n" + "Частота заливки: " + rate.ToString() + " cек." + 
                "\n" + "Размер кисти: " + size.ToString()+" м."+ ((currentObjectPreview)?
                "\n"+"Выбранный объект: "+currentObjectPreview.name:"") +
                "\n" + "Угол поворота: " + currentRotateByY, EditorStyles.helpBox);
        }
        if(modeIndex == 0)
        GUI.Label(new Rect(HandleUtility.WorldToGUIPoint(posBrush), new Vector2(164, 70)), EditorGUIUtility.IconContent("AS Badge New"));//if spawn
        else
        GUI.Label(new Rect(HandleUtility.WorldToGUIPoint(posBrush), new Vector2(164, 70)), EditorGUIUtility.IconContent("AS Badge Delete"));//if delete

        HandleUtility.Repaint();
        Handles.EndGUI();
    }

    void DeletePrefabs(int localMouseClickIndex)
    {
        if (objectsToDelete.Length == 0)
            return;

        for (int i = 0; i < objectsToDelete.Length; i++)
        {
            GameObject objectToDelete = null;
            if (objectsToDelete[i])
            {
                if (objectsToDelete[i].transform.parent && objectsToDelete[i].transform.parent.name == "Objects")
                    objectToDelete = objectsToDelete[i];
                else if (objectsToDelete[i].transform.parent && objectsToDelete[i].transform.parent.parent && objectsToDelete[i].transform.parent.parent.name == "Objects")
                    objectToDelete = objectsToDelete[i].transform.parent.gameObject;
                else if (objectsToDelete[i].transform.parent.parent && objectsToDelete[i].transform.parent.parent.parent && objectsToDelete[i].transform.parent.parent.parent.name == "Objects")
                    objectToDelete = objectsToDelete[i].transform.parent.parent.gameObject;
            }

            GameObject prefab = null;
            bool name = false;

            if (objectToDelete)
            {
                prefab = (GameObject)PrefabUtility.GetPrefabParent(objectToDelete);
                name = (objectToDelete.name != "Field" && objectToDelete.name != "Road" && objectToDelete.name != "CrosswayRoad");
            }

            timer = Time.realtimeSinceStartup;

            DeleteHistory dh;
            if (prefab)
            {
                if (myInstance)
                {
                    if (historyDelete.Count > 0 && historyDelete[historyDelete.Count - 1].mouseClickIndex != localMouseClickIndex && historyDelete.Count == myInstance.limitHistoryActions)
                        historyDelete.Remove(historyDelete[0]);
                }
                else
                {
                    if (historyDelete.Count > 0 && historyDelete[historyDelete.Count - 1].mouseClickIndex != localMouseClickIndex && historyDelete.Count == 10)
                        historyDelete.Remove(historyDelete[0]);
                }

                if (historyDelete.Count == 0 || historyDelete.Count > 0 && historyDelete[historyDelete.Count - 1].mouseClickIndex != localMouseClickIndex)
                    dh = new DeleteHistory();
                else
                    dh = historyDelete[historyDelete.Count - 1];

                dh.actionIndex = 2;
                dh.mouseClickIndex = localMouseClickIndex;
                dh.objectRef.Add(prefab);
                dh.instanceID.Add(objectToDelete.GetInstanceID());
                dh.position.Add(objectToDelete.transform.position);
                dh.rotation.Add(objectToDelete.transform.rotation);
                dh.parent.Add(objectToDelete.transform.parent);
                if (historyDelete.Count == 0 || historyDelete.Count > 0 && historyDelete[historyDelete.Count - 1].mouseClickIndex != localMouseClickIndex)
                    historyDelete.Add(dh);

                if (objectToDelete != null && name)
                    DestroyImmediate(objectToDelete);
            }
        }
    }

    int mouseClickIndex = -1; 

    // Handle the brush's input
    void HandleBrushInput(bool on_surface)
    {

        if (on_surface)
        {
            int control_ID = GUIUtility.GetControlID(_editorHash, FocusType.Passive);
            if (Event.current.button == 0)
            {
                switch (Event.current.type)
                {
                    case EventType.mouseDown:
                        mouseClickIndex++;
                        
                        if (modeIndex == 0)
                            PlacePrefab(mouseClickIndex);
                        else
                                DeletePrefabs(mouseClickIndex);

                        break;

                    case EventType.mouseDrag:

                        if (modeIndex==0)
                        {
                            if (fillMode && Time.realtimeSinceStartup - timer >= rate)
                                PlacePrefab(mouseClickIndex);
                        }
                        else
                        {
                            if (fillMode && Time.realtimeSinceStartup - timer >= rate)
                                    DeletePrefabs(mouseClickIndex);
                        }
                        break;

                    case EventType.mouseUp:
                        Event.current.Use();
                        break;
                }
            }
            if (Event.current.type == EventType.Layout)
                HandleUtility.AddDefaultControl(control_ID);
        }

    }


    void PlacePrefab(int localMouseClickIndex)
    {

        if (HasPrefabs())
        {
            GameObject prefab = prefabsToPaint[currentObjectToSpawnIndex];
            Vector3 position = posBrush;
            Quaternion rotation = GetRotation(position, surfaceNormBrush) * Quaternion.Euler(0, currentRotateByY, 0);

            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(prefab) as GameObject;

            if (myInstance && myInstance.attachObjectsToChank && currentHittedObject)
            {

                if(currentHittedObject.name == "Field")
                {
                   GameObject go = GameObject.Find(currentHittedObject.transform.parent.gameObject.name + "/Objects");
                    obj.transform.parent = go.transform;
                }
                else if (currentHittedObject.name == "Road" || currentHittedObject.name == "CrosswayRoad")
                {
                    GameObject go = GameObject.Find(currentHittedObject.transform.parent.parent.gameObject.name + "/Objects");
                    obj.transform.parent = go.transform;
                }
            }

            obj.transform.position = position;
            obj.transform.rotation = rotation;

            obj.transform.position += surfaceNormBrush * 0 * obj.transform.localScale.y;
            timer = Time.realtimeSinceStartup;

            SpawnHistory sh;

            if (myInstance)
            { 
            if (historySpawn.Count > 0 && historySpawn[historySpawn.Count - 1].mouseClickIndex != localMouseClickIndex && historySpawn.Count == myInstance.limitHistoryActions)
                historySpawn.Remove(historySpawn[0]);
            }
            else
            {
                if (historySpawn.Count > 0 && historySpawn[historySpawn.Count - 1].mouseClickIndex != localMouseClickIndex && historySpawn.Count == 10)
                    historySpawn.Remove(historySpawn[0]);
            }

            if (historySpawn.Count == 0 || historySpawn.Count > 0 && historySpawn[historySpawn.Count - 1].mouseClickIndex != localMouseClickIndex)
                sh = new SpawnHistory();
            else
                sh = historySpawn[historySpawn.Count - 1];

            sh.actionIndex = 1;
            sh.mouseClickIndex = localMouseClickIndex;
            sh.objectRef.Add(prefabsToPaint[currentObjectToSpawnIndex]);
            sh.spawnedObjects.Add(obj);
            sh.instanceID.Add(obj.GetInstanceID());
            sh.position.Add(position);
            sh.rotation.Add(rotation);
            sh.parent.Add(obj.transform.parent);
            if(historySpawn.Count == 0 || historySpawn.Count > 0 && historySpawn[historySpawn.Count - 1].mouseClickIndex != localMouseClickIndex)
                historySpawn.Add(sh);


            currentObjectToSpawnIndex = Random.Range(0, prefabsToPaint.Count);

            if(randomRotatedSpawnObjects)
                    currentRotateByY = Random.Range(0, 360);

            if (currentObjectPreview)
                DestroyImmediate(currentObjectPreview);
        }
        else
        {
            EditorUtility.DisplayDialog("Забыли выбрать объект для рисования?",
            "Выберите хотя бы один объект для рисования. Затем снова нажмите 'Начать рисовать'.", "Понял", "");
            if (enablePaint)
                enablePaint = false;
        }
    }


    Vector3 GetRandPos()
    {
        Ray ray;
        RaycastHit hit;
        bool result;
        int counter = 0;
        const int max_iterations = 30;

        do
        {
            Quaternion rotation = Quaternion.AngleAxis(Random.Range(0f, 360f), surfaceNormBrush);
            Vector3 direction = dirBrush * Random.Range(0.0f, size);
            Vector3 position = posBrush + rotation * direction;

            ray = new Ray(position + surfaceNormBrush, -surfaceNormBrush);

            result = Physics.Raycast(ray, out hit, Mathf.Infinity);

            if (counter++ == max_iterations && !result)
            {
                hit.point = posBrush;
                break;
            }

        } while (!result);

        surfaceNormBrush = hit.normal;

        return hit.point;
    }
}
#endif
